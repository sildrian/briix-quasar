
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') }
    ]
  },
  {
    path: '/movie',
    component: () => import('layouts/movies/MovieLayout.vue'),
    children: [
      { path: '', component: () => import('pages/movies/MovieCollectionPage.vue') },
      { path: 'edit', component: () => import('pages/movies/MovieEditPage.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
