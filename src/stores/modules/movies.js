export default {
    state: {
      list_movies: localStorage.getItem("list_movies") || "",
      detail_movies: localStorage.getItem("detail_movies") || ""
    },
    getters: {
        list_movies: (state) => state.list_movies,
        detail_movies: (state) => state.detail_movies
    },
    mutations: {
      update_listmovies(state, payload){
          state.list_movies = payload
        },
      update_detailmovies(state, payload){
        state.detail_movies = payload
      },
    },
    actions: {
      action_listmovies ({ commit }, payload) {
        console.log(payload)
          payload = JSON.stringify(payload)
          localStorage.setItem("list_movies", payload)
          commit('update_listmovies', payload)
      },
      action_detailmovies ({ commit }, payload) {
        if(payload !== ""){
          payload = JSON.stringify(payload)
        }
        localStorage.setItem("detail_movies", payload)
        commit('update_detailmovies', payload)
    },
    }
  }
  